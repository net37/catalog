/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.catalog.nettigatujuhcatalog.controller;

import com.machfudh.nettigatujuh.catalog.nettigatujuhcatalog.dao.CategoryDao;
import com.machfudh.nettigatujuh.catalog.nettigatujuhcatalog.dao.SubCategoryDao;
import com.machfudh.nettigatujuh.catalog.nettigatujuhcatalog.entity.Category;
import com.machfudh.nettigatujuh.catalog.nettigatujuhcatalog.entity.SubCategory;
import java.util.Date;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author Moh Machfudh
 */
@Controller
public class SubCategoryController {
    
    public static final Logger LOGGER = LoggerFactory.getLogger(CategoryController.class);
    
    @Autowired
    private SubCategoryDao subCategoryDao;
    
    private Date tglsnow = new Date();
    
    @PreAuthorize("hasAuthority('CLIENT_VIEW')")
    @GetMapping("/api/subcategory")
    @ResponseBody
    public Page<SubCategory> dataSubCategory(Pageable page){
        return subCategoryDao.findAll(page);
    }
    
    @PreAuthorize("hasAuthority('USER_EDIT')")
    @PostMapping("/api/subcategory")
    @ResponseStatus(HttpStatus.CREATED)
    public void saveSubCategory(@RequestBody @Valid SubCategory category){        
        tglsnow = new Date();
        category.setInsertdate(Long.toString(tglsnow.getTime()));
        category.setEditdate(Long.toString(tglsnow.getTime()));
        subCategoryDao.save(category);
    }
    
    @PreAuthorize("hasAuthority('USER_EDIT')")
    @PutMapping("api/subcategory/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void editSubCategory(@PathVariable(name = "id") String id, @RequestBody @Valid SubCategory category) {
        SubCategory newCategory = subCategoryDao.findOne(id);

        tglsnow = new Date();
        if (newCategory == null) {
            return;
        }
        BeanUtils.copyProperties(category, newCategory);
        newCategory.setId(id);
        newCategory.setEditdate(Long.toString(tglsnow.getTime()));
        subCategoryDao.save(newCategory);
    }
    
}
