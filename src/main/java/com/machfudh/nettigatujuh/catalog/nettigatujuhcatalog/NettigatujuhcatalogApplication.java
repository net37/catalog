package com.machfudh.nettigatujuh.catalog.nettigatujuhcatalog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class NettigatujuhcatalogApplication {

	public static void main(String[] args) {
		SpringApplication.run(NettigatujuhcatalogApplication.class, args);
	}

}
