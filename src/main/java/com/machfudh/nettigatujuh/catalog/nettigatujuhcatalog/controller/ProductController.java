/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.catalog.nettigatujuhcatalog.controller;

import com.machfudh.nettigatujuh.catalog.nettigatujuhcatalog.dao.ProductDao;
import com.machfudh.nettigatujuh.catalog.nettigatujuhcatalog.entity.Product;
import java.util.Date;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author Moh Machfudh
 */
@Controller
public class ProductController {
    
    public static final Logger LOGGER = LoggerFactory.getLogger(BrandController.class);
    
    @Autowired
    private ProductDao productDao;
    
    private Date tglsnow = new Date();
    
    @PreAuthorize("hasAuthority('CLIENT_VIEW')")
    @GetMapping("/api/product")
    @ResponseBody
    public Page<Product> dataProduct(Pageable page){
        return productDao.findAll(page);
    }
    
    @PreAuthorize("hasAuthority('USER_EDIT')")
    @PostMapping("/api/product")
    @ResponseBody
    public void saveProduct(@RequestBody @Valid Product product){        
        tglsnow = new Date();
        product.setInsertdate(Long.toString(tglsnow.getTime()));
        product.setEditdate(Long.toString(tglsnow.getTime()));
        productDao.save(product);
    }
    
    @PreAuthorize("hasAuthority('USER_EDIT')")
    @PutMapping("api/product/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void editProduct(@PathVariable(name = "id") String id, @RequestBody @Valid Product product) {
        Product newProduct = productDao.findOne(id);

        tglsnow = new Date();
        if (newProduct == null) {
            return;
        }
        BeanUtils.copyProperties(product, newProduct);
        newProduct.setId(id);
        newProduct.setEditdate(Long.toString(tglsnow.getTime()));
        productDao.save(newProduct);
    }
    
    @PreAuthorize("hasAuthority('CLIENT_VIEW')")
    @GetMapping("api/product/{id}")
    @ResponseBody
    public Product productById(@PathVariable(name = "id") String id, Product product){
        return productDao.findOne(id);
    }
}
