/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.catalog.nettigatujuhcatalog.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Moh Machfudh
 */
@Data
@Entity @Table(name="net_product")
public class Product implements Serializable{
    
    @Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id; 
    
    private int ratnum;
    private int  rattotal;
    private String ratuser;
    
        
    @NotNull @NotEmpty
    private String nama;
    @NotNull @NotEmpty
    private String keterangan;
    @NotNull @NotEmpty
    private String tokoid;
    @NotNull @NotEmpty
    private String kategoriid;
    @NotNull @NotEmpty
    private String subkatid;
    @NotNull @NotEmpty
    private String brandid;
    
    private int hargajual;
    private int hargabeli;
    private int ongkir;
    private String tag;
    private String status;
    private int stok;
    private String unit;
    private int diskon;
    private String diskontype;
    private int tax;
    private String taxtype;
    private String warna;
    private String options;
    private String datalain;
    private String updatetime;
    private String lastview;
    private String isbundle;
    private String insertid;
    private String insertdate;
    private String editid;
    private String editdate;
    
}
