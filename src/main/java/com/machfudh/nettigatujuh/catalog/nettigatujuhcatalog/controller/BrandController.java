/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.catalog.nettigatujuhcatalog.controller;

import com.machfudh.nettigatujuh.catalog.nettigatujuhcatalog.dao.BrandDao;
import com.machfudh.nettigatujuh.catalog.nettigatujuhcatalog.entity.Brand;
import java.util.Date;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author Moh Machfudh
 */
@Controller
public class BrandController {
    
    public static final Logger LOGGER = LoggerFactory.getLogger(BrandController.class);
    
    @Autowired
    private BrandDao brandDao;
    
    private Date tglsnow = new Date();
    
    @PreAuthorize("hasAuthority('CLIENT_VIEW')")
    @GetMapping("/api/brand")
    @ResponseBody
    public Page<Brand> dataBrand(Pageable page){
        return brandDao.findAll(page);
    }
    
    @PreAuthorize("hasAuthority('USER_EDIT')")
    @PostMapping("/api/brand")
    @ResponseBody
    public void saveBrand(@RequestBody @Valid Brand brand){   
        tglsnow = new Date();
        brand.setInsertdate(Long.toString(tglsnow.getTime()));
        brand.setEditdate(Long.toString(tglsnow.getTime()));
        brandDao.save(brand);
    }
    
    @PreAuthorize("hasAuthority('USER_EDIT')")
    @PutMapping("api/brand/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void editBrand(@PathVariable(name = "id") String id, @RequestBody @Valid Brand brand) {
        Brand newBrand = brandDao.findOne(id);
        tglsnow = new Date();
        
        if (newBrand == null) {
            return;
        }
        BeanUtils.copyProperties(brand, newBrand);
        newBrand.setId(id);
        newBrand.setEditdate(Long.toString(tglsnow.getTime()));
        brandDao.save(newBrand);
    }
    
    
    
    
}
