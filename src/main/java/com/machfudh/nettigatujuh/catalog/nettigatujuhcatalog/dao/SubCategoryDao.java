/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.catalog.nettigatujuhcatalog.dao;

import com.machfudh.nettigatujuh.catalog.nettigatujuhcatalog.entity.SubCategory;
import java.io.Serializable;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author Moh Machfudh
 */
public interface SubCategoryDao extends PagingAndSortingRepository<SubCategory, String>{
    
}
